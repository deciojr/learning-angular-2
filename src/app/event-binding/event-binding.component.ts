import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {

  value: string;
  savedValue: string;
  isMouseOver: boolean = false;

  constructor() { }

  ngOnInit() {
  }
  clickMe() {
    alert("Button clicked");
  }
  onKeyUp(event: KeyboardEvent) {
    this.value = (<HTMLInputElement>event.target).value;
  }
  saveValue(value: string) {
    this.savedValue = value;
  }
  onMouseOver() {
    this.isMouseOver = !this.isMouseOver;
  }
}
